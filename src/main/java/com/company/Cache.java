package com.company;

import java.lang.reflect.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by me on 17.03.2016.
 */
class Cache {
    Map<Class,Map> m = new HashMap<>();
    private static Cache cacheInstance = null;

    private Cache(){

    }

    public static synchronized Cache getInstance(){
        if(cacheInstance == null ){
            cacheInstance = new Cache();
        }
        return cacheInstance;
    }

    public <KEY,E> Map<KEY,E> getRepo(KEY k){
        if(!m.containsKey(k.getClass())){
            m.put(k.getClass(), new HashMap<KEY,Map<KEY,E>>());
        }
        return m.get(k.getClass());
    }

}
