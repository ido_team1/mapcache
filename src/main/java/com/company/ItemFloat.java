package com.company;


public class ItemFloat implements Entity<Float> {
    private static Float lastId = 0.275F;
    private Float _id;

    private String name;
    private double price;
    private int quantity;

    public ItemFloat(){
        name = "FLOAT " + "null";
        price = 0.0;
        quantity = 0;
        _id = lastId + 1;
    }

    public ItemFloat(String itemName, double itemPrice, int itemQuantity){
        name = "FLOAT " + itemName;
        price = itemPrice;
        quantity = itemQuantity;
        _id = lastId++;
    }

    @Override
    public void setId(Float id) {
        _id = id;
    }

    @Override
    public Float getId() {
        return _id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }


}
