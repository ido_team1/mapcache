package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by me on 17.03.2016.
 */
public class ItemRepo implements Repo<Long,Entity<Long>> {
    private Map<Long,Entity<Long>> repo;

    public ItemRepo(){
        Cache cache = Cache.getInstance();
        repo = cache.getRepo(new Long(1));
    }


    @Override
    public Entity<Long> save(Entity<Long> item) {
        repo.put(item.getId(),item);
        return item;
    }

    @Override
    public Entity<Long> removeById(Long id) {
        if(repo.containsKey(id)){
            Entity<Long> entity = repo.remove(id);
            return entity;
        }
        return null;
    }

    @Override
    public Entity<Long> remove(Entity<Long> item){
        return removeById(item.getId());
    }

    @Override
    public Collection<Entity<Long>> getItems() {
        Collection<Entity<Long>> coll = repo.values();
        return coll;
    }

    @Override
    public Entity<Long> getByID(Long id) {
        if(repo.containsKey(id)){
            Entity<Long> entity = repo.get(id);
            return entity;
        }
        return null;
    }

    @Override
    public Collection<Entity<Long>> filter(Predicate<Entity<Long>> predicate) {

        Collection<Entity<Long>> col = new ArrayList<>();
        for(Entity<Long> e : repo.values()){
            if(predicate.test(e)){
                col.add(e);
            }
        }
        if(col.isEmpty()){
            return null;
        }
        return col;
    }
}
