package com.company.data.db;

/**
 * Created by me on 29.03.2016.
 */
public class Employee {
    private Long id;
    private Position position;
    private Person person;
    private Long salary;


    public Employee(Long id, Position position, Person person, Long salary ) {
        this.position = position;
        this.person = person;
        this.id = id;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Position getPositions() {
        return position;
    }

    public void setPosition(Position positions) {
        this.position = positions;
    }

    public Position getPosition() {
        return position;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append("id: ");
        b.append(id);
        b.append("\nperson: \n\t");
        b.append(person.toString());
        b.append("\npositions: \n\t");
        b.append(position.toString());
        b.append("\nsalary: ");
        b.append(salary);

        return b.toString();
    }
}
