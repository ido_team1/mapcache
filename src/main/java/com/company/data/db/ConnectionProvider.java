package com.company.data.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by me on 29.03.2016.
 */
public class ConnectionProvider {
    public static Connection providePostgreConnection(boolean autoCommit) throws IOException, SQLException {
        InputStream rstream =  ConnectionProvider.class.getClassLoader().getResourceAsStream("connection.properties");
        Properties dbprops = new Properties();
        dbprops.load(rstream);

        String dbUrl = dbprops.getProperty("db.host");
        String dbUser = dbprops.getProperty("db.username");
        String dbPassword = dbprops.getProperty("db.password");
        Connection connection = DriverManager.getConnection(dbUrl,dbUser,dbPassword);
        connection.setAutoCommit(autoCommit);
        return connection;
    }
}
