package com.company;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by me on 17.03.2016.
 */
public interface Repo<ID,E extends Entity> {
    E save(E item);
    E removeById(ID id);
    E remove(E item);
    Collection<E> getItems();
    E getByID(ID id);
    Collection<E> filter(Predicate<E> func);
}
