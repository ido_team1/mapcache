package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by me on 17.03.2016.
 */
public class ItemFloatRepo implements Repo<Float,Entity<Float>> {
    private Map<Float,Entity<Float>> repo;

    public ItemFloatRepo(){
        Cache cache = Cache.getInstance();
        repo = cache.getRepo(new Float(1));
    }


    @Override
    public Entity<Float> save(Entity<Float> item) {
        repo.put(item.getId(),item);
        return item;
    }

    @Override
    public Entity<Float> removeById(Float id) {
        if(repo.containsKey(id)){
            Entity<Float> entity = repo.remove(id);
            return entity;
        }
        return null;
    }

    @Override
    public Entity<Float> remove(Entity<Float> item){
        return removeById(item.getId());
    }


    @Override
    public Collection<Entity<Float>> getItems() {
        Collection<Entity<Float>> coll = repo.values();
        return coll;
    }

    @Override
    public Entity<Float> getByID(Float id) {
        if(repo.containsKey(id)){
            Entity<Float> entity = repo.get(id);
            return entity;
        }
        return null;
    }

    @Override
    public Collection<Entity<Float>> filter(Predicate<Entity<Float>> predicate) {
        Collection<Entity<Float>> col = new ArrayList<>();
        for(Entity<Float> e : repo.values()){
            if(predicate.test(e)){
                col.add(e);
            }
        }
        if(col.isEmpty()){
            return null;
        }
        return col;
    }
}
