package com.company;

/**
 * Created by me on 17.03.2016.
 */
public interface Entity<T> {

    void setId(T id);
    T getId();
}
